import {Injectable, Type, ViewContainerRef} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Overlay, OverlayConfig} from '@angular/cdk/overlay';
import {ComponentPortal} from '@angular/cdk/portal';
import {ConfirmDialogComponent} from '../home/pages/datapage/lager/confirm-dialog/confirm-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class LagerService {
  content: Type<any>;
  context;
  afterClosed$ = new Subject<any>();
  overlayRef: any;
  form_template = [
    {
      "type":"textBox",
      "label":"Name",
    },
    {
      "type":"number",
      "label":"Age"
    }
  ];
  constructor(private http: HttpClient, public overlay: Overlay) { }

  getLager(): Observable<any> {
    return this.http.get( 'https://jsonplaceholder.typicode.com/users');
  }
  deleteEmp(id): Observable<any> {
    return this.http.delete( 'https://jsonplaceholder.typicode.com/users/' + id);
  }
  getResponse(): Observable<any> {
    return this.afterClosed$.asObservable();
  }
  saveEmp(data): Observable<any> {
    return this.http.post('https://jsonplaceholder.typicode.com/posts', data);
  }
  open(data) {
    const config = new OverlayConfig();
    config.positionStrategy = this.overlay.position().global().centerHorizontally();
    config.hasBackdrop = true;
    this.overlayRef = this.overlay.create(config);
    this.overlayRef.backdropClick().subscribe(() => {
      this.close('backdropClick');
    });

    this.overlayRef.attach(new ComponentPortal(ConfirmDialogComponent, data));
  }
  public close(str?) {
    this.afterClosed$.next({ text: str });
    this.overlayRef.dispose();

  }
  delete(employeeList, id){
    return employeeList.filter(x => x.id != id);
  }
}


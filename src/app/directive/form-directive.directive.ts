import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appFormDirective]'
})
export class FormDirectiveDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}

import {Component, ViewEncapsulation} from '@angular/core';
import {Helpers} from './helpers';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent {
  title = 'App';
  constructor(){
    //Helpers.setLoading(true);
  }
}
